﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlickeringLight : MonoBehaviour
{
    public GameObject _Light;
    public GameObject sphere;

    public float minTime;
    public float maxTime;
    public float timer;
    Material sphereMaterial;


    void Start()
    {
        _Light = this.gameObject;
        sphereMaterial = Resources.Load<Material>("SphereMaterial");
        timer = Random.Range(minTime, maxTime);    
    }

    void Update()
    {
        FlickLight();
    }

    void FlickLight()
    {
        if(timer > 0.0f)
            timer -= Time.deltaTime;

        if(timer <= 0.0f)
        {
            _Light.GetComponent<Light>().enabled = !_Light.GetComponent<Light>().enabled;
            timer = Random.Range(minTime, maxTime);
        }
    }
}
